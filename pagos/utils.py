import http.client
import json
import datetime
import pprint
import locale
from .models import *



def createdebt(idSubscription):
    sub = Subscription.objects.get(pk=idSubscription)
    apiKey = "ap-aac7bc233333d4cbb54272a9"
    siExiste = "update"
    host = "staging.adamspay.com"
    path = "/api/v1/debts"
    locale.setlocale(locale.LC_ALL, ("es_ES", "UTF-8"))
    # Hora DEBE ser en UTC!
    inicio_validez = datetime.datetime.utcnow().replace()
    fin_validez = inicio_validez + datetime.timedelta(days=30)

    # Crear modelo de la deuda
    deuda = {
        "docId": sub.pk,
        "amount": {"currency": "PYG", "value": "{}".format(sub.plan.monthly_price)},
        "label": "Cuota mes {}. \n Capellan:{} {}".format(inicio_validez.strftime("%B"), sub.chaplain.name, sub.chaplain.last_name),
        "validPeriod": {
            "start": inicio_validez.strftime("%Y-%m-%dT%H:%M:%S"),
            "end": fin_validez.strftime("%Y-%m-%dT%H:%M:%S")
        }
    }

    # El post debe llevar la deuda en la propiedad "debt"
    post = {"debt": deuda}
    # Crear JSON
    payload = json.JSONEncoder().encode(post).encode("utf-8")

    headers = {"apikey": apiKey, "Content-Type": "application/json", "x-if-exists": siExiste}
    conn = http.client.HTTPSConnection(host)
    conn.request("POST", path, payload, headers)
    data = conn.getresponse().read().decode("utf-8")
    response = json.JSONDecoder().decode(data)
    # Datos retornan en la propiedad "debt"

    pp = pprint.PrettyPrinter(indent=2)
    if "debt" in response:
        debt = response["debt"]
        sub.url = debt["payUrl"]
        sub.save()
        print("Deuda creada exitosamente")
        print("URL=" + debt["payUrl"])
    else:
        print("# Error")
        if "meta" in response:
            pp.pprint(response["meta"])

def deleteDebt(idSubscription):
    deuda = Subscription.objects.get(pk=idSubscription)
    apiKey = "ap-aac7bc233333d4cbb54272a9"
    notificarAlWebhook = "true"
    host = "staging.adamspay.com"
    path = "/api/v1/debts/{}".format(deuda.id)
    headers = {"apikey": apiKey, "x-should-notify": notificarAlWebhook}

    conn = http.client.HTTPSConnection(host)
    conn.request("DELETE", path, "", headers)
    data = conn.getresponse().read().decode("utf-8")

    response = json.JSONDecoder().decode(data)

    pp = pprint.PrettyPrinter(indent=2)
    if "debt" in response:
        debt = response["debt"]
        print("Deuda borrada")
        print("Detalles:")
        pp.pprint(response["debt"])
    else:
        print("# Error")
        if "meta" in response:
            pp.pprint(response["meta"])
