from .views import *
from django.urls import path

urlpatterns = [
    # PLAN
    path('plan/list/', PlanDatatableView.as_view(), name='plan_list'),
    path('plan/details/<int:pk>', PlanDetailsView.as_view(), name='plan_details'),
    path('plan/create/', PlanCreateView.as_view(), name='plan_create'),
    path('plan/update/<int:pk>', PlanUpdateView.as_view(), name='plan_update'),
    path('plan/delete/<int:pk>', PlanDeleteView.as_view(), name='plan_delete'),
    # PAYMENT HISTORY
    path('payment_history/list/', PaymentHistoryDatatableView.as_view(), name='payment_history_list'),
    path('payment_history/details/<int:pk>', PaymentHistoryDetailsView.as_view(), name='payment_history_details'),
    path('payment_history/create/', PaymentHistoryCreateView.as_view(), name='payment_history_create'),
    path('payment_history/update/<int:pk>', PaymentHistoryUpdateView.as_view(), name='payment_history_update'),
    path('payment_history/delete/<int:pk>', PaymentHistoryDeleteView.as_view(), name='payment_history_delete'),
    # SUBSCRIPTIONS
    path('subscriptions/list/', SubscriptionDatatableView.as_view(), name='subscriptions_list'),
    path('subscriptions/details/<int:pk>', SubscriptionDetailsView.as_view(), name='subscriptions_details'),
    path('subscriptions/create/', SubscriptionCreateView.as_view(), name='subscriptions_create'),
    path('subscriptions/update/<int:pk>', SubscriptionUpdateView.as_view(), name='subscriptions_update'),
    path('subscriptions/delete/<int:pk>', SubscriptionDeleteView.as_view(), name='subscriptions_delete'),
    path('subscriptions/adetails/', SubscriptionADetailsView.as_view(), name='suscriptions_adetails'),
    # API
    path('api/webhook/', WebHook.as_view(), name='webhook'),
]
