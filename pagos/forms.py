from datetime import datetime
from django import forms
from django.forms import ModelForm, inlineformset_factory
from .models import *
from crispy_forms.layout import Layout, Div, Row, Column, Field, Fieldset
from crispy_forms.helper import FormHelper
from django.utils.translation import ugettext_lazy as _


# Plan Creation Form
class PlanForm(ModelForm):
    name = forms.CharField(label=_('Name'))
    description = forms.CharField(label=_('Description'))
    monthly_price = forms.CharField(label=_('Monthly Price'))
    yearly_price = forms.CharField(label=_('Yearly Price'))

    class Meta:
        model = Plan
        fields = [
            'name',
            'description',
            'monthly_price',
            'yearly_price',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class PaymentHistoryForm(ModelForm):
    payment_type = forms.ChoiceField(choices=PaymentHistory.choices,
                                     widget=forms.Select,
                                     label=_('Payment Type'))
    date = forms.DateField(
        required=True,
        label=_('Payment History Date'),
        widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'}),
        initial=datetime.datetime.today().strftime('%d/%m/%Y'),
    )
    amount = forms.CharField(label=_('Amount'))
    ref_number = forms.CharField(label=_('Reference Number'))
    subscription = forms.ModelChoiceField(queryset=Subscription.objects.all(),
                                     widget=forms.Select,
                                     label=_('Subscription'))
    class Meta:
        model = PaymentHistory
        fields = [
            'payment_type',
            'date',
            'amount',
            'ref_number',
            'subscription',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['date'].input_formats = ['%d/%m/%Y']

class SubscriptionForm(ModelForm):
    chaplain = forms.ModelChoiceField(queryset=Chaplain.objects.all(),
                                      widget=forms.Select,
                                      label=_('Chaplain'))
    plan = forms.ModelChoiceField(queryset=Plan.objects.all(),
                                  widget=forms.Select,
                                  label=_('Plan'))
    startDate = forms.DateField(label=_('Start Date'),
                                widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'}),)
    endDate = forms.DateField(label=_('End Date'), required=False,
                              widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'}),)
    finishDate = forms.DateField(label=_('Finish Date'), required=False,
                                 widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'}),)
    isActive = forms.BooleanField(label=_('Is Active?'))

    class Meta:
        model = Subscription
        fields = [
            'chaplain',
            'plan',
            'startDate',
            'endDate',
            'finishDate',
            'isActive',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['startDate'].input_formats = ['%d/%m/%Y']
        self.fields['endDate'].input_formats = ['%d/%m/%Y']
        self.fields['finishDate'].input_formats = ['%d/%m/%Y']
