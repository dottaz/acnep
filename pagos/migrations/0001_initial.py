# Generated by Django 3.2.6 on 2022-02-19 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('chaplains', '0002_auto_20220219_1844'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentHistory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('payment_type', models.CharField(choices=[('Efectivo', '1'), ('Billetera Electronica', '2'), ('Transferencia Bancaria', '3'), ('Deposito Bancario', '4'), ('Tarjeta Credito', '5'), ('Tarjeta Debito', '6'), ('AdamsPay', '7')], max_length=25, verbose_name='Payment Type')),
                ('date', models.DateField()),
                ('amount', models.FloatField(verbose_name='Amount')),
                ('ref_number', models.CharField(max_length=30, verbose_name='Reference Number')),
            ],
            options={
                'permissions': (('can_undelete', 'Can undelete this object'),),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('description', models.CharField(max_length=100, verbose_name='Description')),
                ('monthly_price', models.IntegerField(verbose_name='Monthly Price')),
                ('yearly_price', models.IntegerField(verbose_name='Monthly Price')),
            ],
            options={
                'verbose_name': 'Plan',
                'verbose_name_plural': 'Plans',
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('startDate', models.DateField()),
                ('endDate', models.DateField()),
                ('finishDate', models.DateField()),
                ('isActive', models.BooleanField(default=True)),
                ('chaplain', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subscriptions', to='chaplains.chaplain', verbose_name='Chaplain')),
                ('payment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='subscriptions', to='pagos.paymenthistory', verbose_name='Payment History')),
                ('plan', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='subscriptions', to='pagos.plan', verbose_name='Plan')),
            ],
            options={
                'verbose_name': 'Subscription',
                'verbose_name_plural': 'Subscriptions',
            },
        ),
    ]
