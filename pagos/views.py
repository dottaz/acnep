from datatableview.views import DatatableView
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from datatableview import Datatable, columns
from datetime import date, datetime
from django.db import transaction
from django.template import RequestContext, loader
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import *
from rest_framework.views import APIView

from .forms import *
from .models import *
from .utils import *

# Create your views here.

# PLAN

# Datatable Class Plan
class PlanDatatable(Datatable):
    actions = columns.DisplayColumn(_('Actions'), processor='get_actions')

    class Meta:
        model = Plan
        columns = [
            'actions',
            'name',
            'description',
            'monthly_price',
            'yearly_price',
        ]
        search_fields = [
            'name',
            'description',
            'monthly_price',
            'yearly_price',
        ]
        structure_template = "datatableview/bootstrap_structure.html"
        ordering = ['-id']

    @staticmethod
    def get_actions(instance, view, *args, **kwargs):
        rid = int(instance.pk)
        detail_url = reverse('plan_details', args=[rid])
        update_url = reverse('plan_update', args=[rid])
        delete_url = reverse('plan_delete', args=[rid])
        return """
                      <div class="container">
                          <div class="row">
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-eye"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-pencil-alt"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-trash"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                      """.format(detail_url, update_url, delete_url)


# Vista DatatableView de Planes
class PlanDatatableView(LoginRequiredMixin, DatatableView):
    model = Plan
    datatable_class = PlanDatatable
    template_name = 'plan/plan_list.html'


# Vista Detalles de Planes
class PlanDetailsView(LoginRequiredMixin, DetailView):
    model = Plan
    template_name = 'plan/plan_detail.html'
    context_object_name = 'plan'


# Vista Actualizacion de Item
class PlanUpdateView(LoginRequiredMixin, UpdateView):
    model = Plan
    template_name = 'plan/plan_update.html'
    form_class = PlanForm
    context_object_name = 'plan'

    def form_invalid(self, form):
        print("Intento de actualizacion de Item fallido")
        error = form.errors
        print(error)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('plan_list')


# Vista Eliminacion de Item
class PlanDeleteView(LoginRequiredMixin, DeleteView):
    model = Plan
    template_name = 'plan/plan_delete.html'
    context_object_name = 'plan'

    def post(self, request, *args, **kwargs):
        plan_id = kwargs['pk']
        plan = get_object_or_404(Plan, pk=plan_id)
        plan.delete()
        messages.success(self.request, "El registro fue eliminado de forma satisfactoria.")
        return HttpResponseRedirect(reverse('plan_list'))


# Vista Creacion de Item
class PlanCreateView(LoginRequiredMixin, CreateView):
    model = Plan
    form_class = PlanForm
    template_name = 'plan/plan_create.html'

    def get_context_data(self, **kwargs):
        context = super(PlanCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        item = form.save()
        return HttpResponseRedirect(reverse('plan_list'))

    def get_success_url(self):
        return reverse('plan_list')


# PAYMENT HISTORY


# Datatable Class Payment History
class PaymentHistoryDatatable(Datatable):
    actions = columns.DisplayColumn(_('Actions'), processor='get_actions')

    class Meta:
        model = PaymentHistory
        columns = [
            'actions',
            'id',
            'payment_type',
            'date',
            'amount',
            'ref_number',
        ]
        search_fields = [
            'payment_type',
            'date',
            'amount',
            'ref_number',
        ]
        structure_template = "datatableview/bootstrap_structure.html"
        ordering = ['-id']

    @staticmethod
    def get_actions(instance, view, *args, **kwargs):
        rid = int(instance.pk)
        detail_url = reverse('payment_history_details', args=[rid])
        update_url = reverse('payment_history_update', args=[rid])
        delete_url = reverse('payment_history_delete', args=[rid])
        return """
                      <div class="container">
                          <div class="row">
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-eye"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-pencil-alt"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-trash"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                      """.format(detail_url, update_url, delete_url)


# Vista DatatableView de PaymentHistory
class PaymentHistoryDatatableView(LoginRequiredMixin, DatatableView):
    model = PaymentHistory
    datatable_class = PaymentHistoryDatatable
    template_name = 'paymentHistory/payment_history_list.html'


# Vista Detalles de PaymentHistory
class PaymentHistoryDetailsView(LoginRequiredMixin, DetailView):
    model = PaymentHistory
    template_name = 'paymentHistory/payment_history_detail.html'
    context_object_name = 'payment_history'


# Vista Actualizacion de PaymentHistory
class PaymentHistoryUpdateView(LoginRequiredMixin, UpdateView):
    model = PaymentHistory
    template_name = 'paymentHistory/payment_history_update.html'
    form_class = PaymentHistoryForm
    context_object_name = 'payment_history'

    def form_invalid(self, form):
        print("Intento de actualizacion de registro fallido")
        error = form.errors
        print(error)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('payment_history_list')


# Vista Eliminacion de PaymentHistory
class PaymentHistoryDeleteView(LoginRequiredMixin, DeleteView):
    model = PaymentHistory
    template_name = 'paymentHistory/payment_history_delete.html'
    context_object_name = 'payment_history'

    def post(self, request, *args, **kwargs):
        payment_history_id = kwargs['pk']
        payment_history = get_object_or_404(PaymentHistory, pk=payment_history_id)
        payment_history.delete()
        messages.success(self.request, "El registro fue eliminado de forma satisfactoria.")
        return HttpResponseRedirect(reverse('payment_history_list'))


# Vista Creacion de PaymentHistory
class PaymentHistoryCreateView(LoginRequiredMixin, CreateView):
    model = PaymentHistory
    form_class = PaymentHistoryForm
    template_name = 'paymentHistory/payment_history_create.html'

    def get_context_data(self, **kwargs):
        context = super(PaymentHistoryCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        payment = form.save()
        debt = payment.subscription
        try:
            subscription = get_object_or_404(Subscription, pk=debt.id)
            subscription.cancelsubscription()
            payment.save()
            return HttpResponseRedirect(reverse('payment_history_list'))
        except:
            return self.render_to_response(self.get_context_data(form=form))


    def get_success_url(self):
        return reverse('payment_history_list ')


# SUBSCRIPTIONS


# Datatable Class Subscriptions
class SubscriptionsDatatable(Datatable):
    actions = columns.DisplayColumn(_('Actions'), processor='get_actions')

    class Meta:
        model = Subscription
        columns = [
            'actions',
            'chaplain',
            'plan',
            'startDate',
            'endDate',
            'finishDate',
            'isActive',
        ]
        search_fields = [
            'chaplain',
            'plan',
            'startDate',
            'endDate',
            'finishDate',
            'isActive',
        ]
        structure_template = "datatableview/bootstrap_structure.html"
        ordering = ['-id']

    @staticmethod
    def get_actions(instance, view, *args, **kwargs):
        rid = int(instance.pk)
        detail_url = reverse('subscriptions_details', args=[rid])
        update_url = reverse('subscriptions_update', args=[rid])
        delete_url = reverse('subscriptions_delete', args=[rid])
        return """
                      <div class="container">
                          <div class="row">
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-eye"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-pencil-alt"></i>
                                  </a>
                              </div>
                              <div class="col" style="width: 80px; ">
                                  <a href="{}" class="btn btn-info btn-circle">
                                      <i class="fas fa-trash"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                      """.format(detail_url, update_url, delete_url)


# Vista DatatableView de Subscription
class SubscriptionDatatableView(LoginRequiredMixin, DatatableView):
    model = Subscription
    datatable_class = SubscriptionsDatatable
    template_name = 'subscriptions/subscriptions_list.html'


# Vista Detalles de Subscription
class SubscriptionDetailsView(DetailView):
    model = Subscription
    template_name = 'subscriptions/subscriptions_detail.html'
    context_object_name = 'subscription'


# Vista Actualizacion de Subscription
class SubscriptionUpdateView(LoginRequiredMixin, UpdateView):
    model = Subscription
    template_name = 'subscriptions/subscriptions_update.html'
    form_class = SubscriptionForm
    context_object_name = 'subscription'

    @transaction.atomic
    def form_valid(self, form):
        context = self.get_context_data()
        item = form.save()
        createdebt(item.id)
        messages.success(self.request, "El registro fue actualizado de forma satisfactoria.")
        return HttpResponseRedirect(reverse('subscriptions_list'))

    @transaction.atomic
    def form_invalid(self, form):
        print("Intento de actualizacion de registro fallido")
        error = form.errors
        print(error)
        return self.render_to_response(self.get_context_data(form=form))

    @transaction.atomic
    def get_success_url(self):
        return reverse('subscriptions_list')


# Vista Eliminacion de Subscription
class SubscriptionDeleteView(LoginRequiredMixin, DeleteView):
    model = Subscription
    template_name = 'subscriptions/subscriptions_delete.html'
    context_object_name = 'subscription'

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        subscription_id = kwargs['pk']
        subscription = get_object_or_404(Subscription, pk=subscription_id)
        deleteDebt(subscription_id)
        subscription.delete()
        messages.success(self.request, "El registro fue eliminado de forma satisfactoria.")
        return HttpResponseRedirect(reverse('subscriptions_list'))


# Vista Detalles de Subscription
class SubscriptionADetailsView(DetailView):
    model = Subscription
    template_name = 'subscriptions/subscriptions_detail.html'
    context_object_name = 'subscription'

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        subscription_id = request.GET.get('doc_id')
        url = (reverse('subscriptions_details', kwargs={'pk': subscription_id}))
        return HttpResponseRedirect(url)

# Vista Creacion de Subscription
class SubscriptionCreateView(LoginRequiredMixin, CreateView):
    model = Subscription
    form_class = SubscriptionForm
    template_name = 'subscriptions/subscriptions_create.html'

    @transaction.atomic
    def get_context_data(self, **kwargs):
        context = super(SubscriptionCreateView, self).get_context_data(**kwargs)
        return context

    @transaction.atomic
    def form_valid(self, form):
        context = self.get_context_data()
        item = form.save()
        createdebt(item.id)
        url = (reverse('subscriptions_details', kwargs={'pk': item.id}))
        messages.success(self.request, "El registro fue creado de forma satisfactoria.")
        return HttpResponseRedirect(url)

    # @transaction.atomic
    # def get_success_url(self):
    #     return reverse('subscriptions_list')


# Clase para WebHook API
class WebHook(APIView):
    model = PaymentHistory


    def post(self, request, *args, **kwargs):
        req = json.loads(request.body)
        notify = req["notify"]
        print(notify)
        return None
