import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from auditlog.registry import auditlog
from softdelete.models import SoftDeleteObject
from base.models import TimeStampModel

# Create your models here.
from chaplains.models import Chaplain


class Plan(SoftDeleteObject, TimeStampModel):
    name = models.CharField(max_length=50,
                            verbose_name=_('Name'),
                            null=False, blank=False)
    description = models.CharField(max_length=100,
                                   verbose_name=_('Description'),
                                   null=False, blank=False)
    monthly_price = models.IntegerField(
        verbose_name=_('Monthly Price'),
        null=False, blank=False
    )
    yearly_price = models.IntegerField(
        verbose_name=_('Monthly Price'),
        null=False, blank=False
    )

    class Meta:
        verbose_name = _('Plan')
        verbose_name_plural = _('Plans')

    def __str__(self):
        return str(self.name)


auditlog.register(Plan)


# SUBSCRIPTIONS
class Subscription(SoftDeleteObject, TimeStampModel):
    chaplain = models.ForeignKey(to=Chaplain, on_delete=models.PROTECT,
                                 verbose_name=_('Chaplain'),
                                 null=False, blank=False,
                                 related_name='subscriptions')
    plan = models.ForeignKey(to=Plan, on_delete=models.PROTECT,
                             verbose_name=_('Plan'),
                             null=False, blank=False,
                             related_name='subscriptions')
    startDate = models.DateField(verbose_name=_('Start Date'),
                             null=False, blank=False,)
    endDate = models.DateField(verbose_name=_('End Date'),
                             null=True, blank=True,)
    finishDate = models.DateField(verbose_name=_('Finish Date'),
                             null=True, blank=True,)
    isActive = models.BooleanField(default=False, null=True, blank=True)
    url = models.URLField(null=True, blank=True)

    class Meta:
        verbose_name = _('Subscription')
        verbose_name_plural = _('Subscriptions')

    def __str__(self):
        return str(self.chaplain.name + ' - ' + str(self.plan.monthly_price))

    def cancelsubscription(self):
        self.isActive = False
        self.finishDate = datetime.datetime.now()
        print("Cancelado correctamente")
        self.save()
        return self


auditlog.register(Subscription)


# PAYMENT HISTORY
class PaymentHistory(SoftDeleteObject, TimeStampModel):
    choices = (
        ('1', 'Efectivo'),
        ('2', 'Billetera Electronica'),
        ('3', 'Transferencia Bancaria'),
        ('4', 'Deposito Bancario'),
        ('5', 'Tarjeta Credito'),
        ('6', 'Tarjeta Debito'),
        ('7', 'AdamsPay'),
    )
    payment_type = models.CharField(max_length=25, choices=choices,
                                    verbose_name=_('Payment Type'),
                                    null=False, blank=False)
    date = models.DateField()
    amount = models.FloatField(verbose_name=_('Amount'), null=False, blank=False)
    ref_number = models.CharField(max_length=30,
                                  verbose_name=_('Reference Number'),
                                  null=False, blank=False)
    subscription = models.ForeignKey(to=Subscription, on_delete=models.PROTECT,
                                     verbose_name=_('Payment History'),
                                     null=True, blank=True,
                                     related_name='payment')


auditlog.register(PaymentHistory)
