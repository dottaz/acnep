import datetime

from django.db import models
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from auditlog.registry import auditlog
from django_countries.fields import CountryField
from django.contrib.auth.models import User
from base.models import Person
# Create your models here.
from softdelete.models import SoftDeleteObject


class Chaplain(Person):
    CIVIL_STATES = (
        (1, "Soltero/a"),
        (2, "Casado/a"),
        (3, "Viudo/a"),
        (4, "Divorciado/a"),
        (5, "Concubino/a"),
        (None, "No seleccionado")
    )
    RANGES = (
        ("Alferes Capellán Superior", "Alferes Capellán Superior"),
        ("Capellán Nacional", "Capellán Nacional"),
        ("Obispo Capellán General", "Obispo Capellán General"),
        ("Obispo Capellán Superior", "Obispo Capellán Superior"),
    )
    civil = models.IntegerField(
        verbose_name=_('Civil State'),
        choices=CIVIL_STATES,
        null=True, blank=True,
        default=None
    )
    country = CountryField(
        verbose_name=_('Nationality'),
        null=True, blank=True
    )
    reference = models.CharField(
        verbose_name=_('Reference'),
        null=True, blank=True,
        max_length=40,
    )
    user = models.ForeignKey(
        to=User,
        verbose_name=_('User'),
        related_name='chaplain',
        null=True, blank=True,
        on_delete=models.PROTECT,
    )
    image = models.ImageField(blank=True, null=True, upload_to='photos/%Y/%m/%d/')
    range = models.CharField(
        verbose_name=_('Range'),
        max_length=100,
        choices=RANGES,
        blank=True, null=True
    )
    destacamento = models.CharField(verbose_name=_('Destacamento'),
                                    blank=True, null=True,
                                    max_length=100)
    admission_date = models.DateField(verbose_name=_('Admission Date'),
                                      blank=True, null=True )

    class Meta:
        verbose_name = _('Chaplain')
        verbose_name_plural = _('Chaplains')

    def __str__(self):
        return self.name + ' ' + self.last_name


auditlog.register(Chaplain)
