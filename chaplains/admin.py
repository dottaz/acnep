from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import *
# Register your models here.

class ChaplainResource(resources.ModelResource):

    class Meta:
        model = Chaplain

class ChaplainAdmin(ImportExportModelAdmin):
    resource_class = ChaplainResource
    search_fields = [
        'id',
        'name',
        'last_name',
        'birth_date',
        'sex',
        'document_number',
        'phone_number',
    ]
    list_filter = [
        'updated_at',
        'created_at',
        'id',
        'name',
        'last_name',
        'birth_date',
        'sex',
        'document_number',
        'phone_number',
    ]

admin.site.register(Chaplain, ChaplainAdmin)
