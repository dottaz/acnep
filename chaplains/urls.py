from .views import *
from django.urls import path

urlpatterns = [
    path('list/', ChaplainsDatatableView.as_view(), name='chaplain_list'),
    path('details/<int:pk>', ChaplainDetailsView.as_view(), name='chaplain_detail'),
    path('create/', ChaplainCreationView.as_view(), name='chaplain_create'),
    path('update/<int:pk>', ChaplainUpdateView.as_view(), name='chaplain_update'),
    path('delete/<int:pk>', ChaplainDeleteView.as_view(), name='chaplain_delete'),
    path('card/<int:pk>', ChaplainCardView.as_view(), name='chaplain_card'),
]