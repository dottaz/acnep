from datetime import datetime
from django import forms
from django.forms import ModelForm
from django_countries.fields import CountryField

from .models import *
from crispy_forms.layout import Layout, Div, Row, Column, Field
from crispy_forms.helper import FormHelper
from django.utils.translation import ugettext_lazy as _

# Chaplain Form


class ChaplainForm(ModelForm):
    country = CountryField().formfield(blank=True, blank_label=_('Choose Country'), required=False)
    name = forms.CharField(label=_('Name'))
    last_name = forms.CharField(label=_('Last Name'))
    birth_date = forms.DateField(
        required=False,
        label=_('Birth Date'),
        widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'})
    )
    sex = forms.ChoiceField(
        widget=forms.Select,
        choices=Chaplain.SEX_CODES,
        required=False
    )
    email = forms.EmailField(label=_('Email'), required=False)
    document_number = forms.CharField(label=_('Document Number'))
    address = forms.CharField(label=_('Address'), required=False)
    city = forms.ChoiceField(
        widget=forms.Select,
        choices=Chaplain.CITYS,
        required=False
    )
    phone_number = forms.CharField(label=_('Phone Number'),required=False)
    civil = forms.ChoiceField(
        widget=forms.Select,
        choices=Chaplain.CIVIL_STATES,
        required=False,
    )
    reference = forms.CharField(label=_('Reference'), required=False)
    image = forms.ImageField(required=False)
    destacamento = forms.CharField(label=_('Destacamento'), required=False)
    admission_date = forms.DateField(
        required=True,
        label=_('Admission Date'),
        widget=forms.DateInput(format='%d/%m/%Y', attrs={'placeholder': 'dd/mm/aaaa'})
    )
    range = forms.ChoiceField(
        widget=forms.Select,
        choices=Chaplain.RANGES,
        required=True
    )
    class Meta:
        model = Chaplain
        fields = [
            'name',
            'last_name',
            'birth_date',
            'sex',
            'email',
            'document_number',
            'address',
            'city',
            'phone_number',
            'civil',
            'country',
            'reference',
            'image',
            'range',
            'destacamento',
            'admission_date'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['birth_date'].input_formats = ['%d/%m/%Y']
        self.fields['admission_date'].input_formats = ['%d/%m/%Y']
