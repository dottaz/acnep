from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.shortcuts import get_object_or_404

from chaplains.models import Chaplain

class Command(BaseCommand):
    help = 'Create users for chaplains'

    def handle(self, *args, **options):
        for chap in Chaplain.objects.all():
            try:
                user = User.objects.create_user(chap.document_number, None, chap.document_number,
                                                first_name=chap.name, last_name=chap.last_name)
                username = get_object_or_404(User, username=chap.document_number)
                chap.user = username
                chap.save()
                self.stdout.write('User created for chaplain "%s"' % chap.__str__())
            except:
                raise CommandError('No se pudo crear el usuario con el capellan "%s"' % chap.id)
