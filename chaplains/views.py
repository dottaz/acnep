from datatableview.views import DatatableView
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from datatableview import Datatable, columns
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import *
from .forms import *
from .models import *
from django.core.mail import send_mail
from acnep import settings as SETTINGS
# Create your views here.

class ChaplainDatatable(Datatable):
    actions = columns.DisplayColumn(_('Actions'), processor='get_actions')
    city = columns.DisplayColumn(_('City'), processor='get_city')
    civil = columns.DisplayColumn(_('Civil State'), processor='get_civil')
    sex = columns.DisplayColumn(_('Sex'), processor='get_sex')
    class Meta:
        model = Chaplain
        columns = [
            'actions',
            'document_number',
            'name',
            'last_name',
            'sex',
            'email',
            'phone_number',
            'address',
            'country',
            'city',
            'civil'
        ]
        search_fields = [
            'document_number',
            'name',
            'last_name',
            'email',
            'phone_number',
            'city',
            'civil',
        ]
        structure_template = "datatableview/bootstrap_structure.html"
        ordering = ['-id']

    @staticmethod
    def get_actions(instance, view, *args, **kwargs):
        rid = int(instance.pk)
        detail_url = reverse('chaplain_detail', args=[rid])
        update_url = reverse('chaplain_update', args=[rid])
        delete_url = reverse('chaplain_delete', args=[rid])
        return """
                <div class="container">
                    <div class="row">
                        <div class="col " style="width: 80px; ">
                            <a href="{}" class="btn btn-info btn-circle">
                                <i class="fas fa-eye"></i>
                            </a>
                        </div>
                        <div class="col " style="width: 80px; ">
                            <a href="{}" class="btn btn-info btn-circle">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        </div>
                        <div class="col " style="width: 80px; ">
                            <a href="{}" class="btn btn-info btn-circle">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
                """.format(detail_url, update_url, delete_url)

    # @staticmethod
    # def get_birth_date(instance, view, *args, **kwargs):
    #     return datetime.strftime(instance.birth_date, '%d/%m/%Y')

    @staticmethod
    def get_sex(instance, view, *args, **kwargs):
        return instance.get_sex_display()

    @staticmethod
    def get_civil(instance, view, *args, **kwargs):
        return instance.get_civil_display()

    @staticmethod
    def get_city(instance, view, *args, **kwargs):
        return instance.get_city_display()


class ChaplainsDatatableView(LoginRequiredMixin, DatatableView):
    model = Chaplain
    datatable_class = ChaplainDatatable
    template_name = 'chaplains/chaplains_list.html'


class ChaplainDetailsView(LoginRequiredMixin, DetailView):
    model = Chaplain
    template_name = 'chaplains/chaplains_detail.html'
    context_object_name = 'chaplain'

    #for commit

class ChaplainCardView(DetailView):
    model = Chaplain
    template_name = 'chaplains/chaplain_card.html'
    context_object_name = 'chaplain'


class ChaplainUpdateView(LoginRequiredMixin, UpdateView):
    model = Chaplain
    template_name = 'chaplains/chaplains_update.html'
    form_class = ChaplainForm
    context_object_name = 'chaplain'

    def form_invalid(self, form):
        print("Actualizacion de capellan no valida")
        error = form.errors
        print(error)
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        if self.request.user.is_staff:
            return reverse('chaplain_list')
        else:
            return reverse('index')


class ChaplainDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'chaplains/chaplains_delete.html'
    model = Chaplain
    context_object_name = 'chaplain'

    def post(self, request, *args, **kwargs):
        chaplain_id = kwargs['pk']
        chaplain = get_object_or_404(Chaplain, pk=chaplain_id)
        chaplain.delete()
        messages.success(self.request, "El registro fue eliminado de forma satisfactoria.")
        return HttpResponseRedirect(reverse('chaplain_list'))


class ChaplainCreationView(LoginRequiredMixin, CreateView):
    model = Chaplain
    form_class = ChaplainForm
    template_name = 'chaplains/chaplain_create.html'

    def get_context_data(self, **kwargs):
        context = super(ChaplainCreationView, self).get_context_data(**kwargs)
        return context

    def form_invalid(self, form):
        context = self.get_context_data()
        print(form.errors)

    def form_valid(self, form):
        context = self.get_context_data()
        chaplain = form.save()
        if chaplain.email:
            mail = chaplain.email
        else:
            mail = None
        passwd=User.objects.make_random_password() # Genera contraseña
        user = User.objects.create_user(chaplain.document_number, mail, passwd)
        username = get_object_or_404(User, username=chaplain.document_number)
        chaplain.user=username
        chaplain.save()
        if chaplain.email:
            send_mail('Creacion de usuario en sistema ACNEP de forma exitosa',
                      'El usuario {} fue creado con la siguiente contraseña {}.'.format(username.username,passwd),
                      'noreply.acnep@gmail.com',
                      ['{}'.format(chaplain.email)],
                      fail_silently=False,
                      auth_user=SETTINGS.EMAIL_HOST_USER,
                      auth_password=SETTINGS.EMAIL_HOST_PASSWORD)
            messages.success(self.request, "El usuario creado y notificado de forma exitosa.")
        # Implementar reset password
        else:
            messages.success(self.request, "El usuario creado de forma exitosa. "
                                           "Favor solicitar creacion de correo para enviar contraseña.")
        return HttpResponseRedirect(reverse('chaplain_list'))

    def get_success_url(self):
        return reverse('chaplain_list')
