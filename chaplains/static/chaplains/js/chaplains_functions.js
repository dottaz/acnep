function initializeInputs() {
    $('#sidebarToggle').trigger('click');
    $('#id_birth_date').mask("00/00/0000");
    $('#id_admission_date').mask("00/00/0000");
    $('#id_birth_date').click(function () {
        console.log('click en fecha de nacimiento');
        $(this).select();
    });
    /* Datetime picker */
    jQuery.datetimepicker.setLocale('es');
    $('#id_image').dropify(
            {
                messages: {
                    'default': 'Haga click o arrastre un archivo',
                    'replace': 'Haga click o arrastre un archivo para reemplazarlo',
                    'remove':  'Eliminar',
                    'error':   'Ocurrió un error.'
                },
                error: {
                    'fileSize': 'El tamaño del archivo es muy grande ({{ value }} max).',
                    'minWidth': 'El ancho de la imagen es muy pequeña ({{ value }}}px min).',
                    'maxWidth': 'El ancho de la imagen es muy grande ({{ value }}}px max).',
                    'minHeight': 'El alto de la imagen es muy pequeño  ({{ value }}}px min).',
                    'maxHeight': 'El alto de la imagen es muy grande  ({{ value }}px max).',
                    'imageFormat': 'Formato de imagen no premitido ({{ value }} solamente).'
                }
            }
        );

    $('#id_birth_date').datetimepicker({
        i18n: {
            es: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa",
                ]
            }
        },
        datepicker: true,
        timepicker: false,
        lang: 'es',
        locale: 'es',
        format: 'd/m/Y',
        inline: false,
    });
    $('#id_admission_date').datetimepicker({
        i18n: {
            es: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                ],
                dayOfWeek: [
                    "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa",
                ]
            }
        },
        datepicker: true,
        timepicker: false,
        lang: 'es',
        locale: 'es',
        format: 'd/m/Y',
        inline: false,
    });
}
