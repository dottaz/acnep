from django.apps import AppConfig


class ChaplainsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chaplains'
